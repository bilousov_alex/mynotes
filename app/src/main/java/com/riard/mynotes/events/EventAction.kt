package com.riard.mynotes.events

class EventAction(val actionType: Int) {

    var selectedNotesCount = 0

    constructor(actionType: Int, selectedNotesCount: Int) : this(actionType) {
        this.selectedNotesCount = selectedNotesCount
    }
}
