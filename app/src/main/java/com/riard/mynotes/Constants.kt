package com.riard.mynotes

class Constants {

    companion object {

        const val NOTES_LIST_FRAGMENT = 0
        const val NOTE_FRAGMENT = 1
        const val FILE_NAME = "PREFERENCES"
        const val COLOR_ID = "COLOR_ID"
        const val LANGUAGE_KEY = "language_key"
        const val OPEN_KEYBOARD = "OPEN_KEYBOARD"
        const val ID = "ID"
        const val FOLDER = "LABEL"
        const val FOLDER_FOR_NEW_NOTE = "FOLDER_FOR_NEW_NOTE"
        const val FILTER = "FILTER"
        const val FOLDER_COLOR_ID = "FOLDER_COLOR_ID"
        const val CURRENT_FOLDER = "CURRENT_FOLDER"

        const val SELECT_MODE_ON = 0
        const val SELECT_MODE_OFF = 1
        const val SELECTED_NOTES_COUNT = 2

    }
}