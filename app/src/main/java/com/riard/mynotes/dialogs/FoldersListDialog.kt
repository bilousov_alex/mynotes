package com.riard.mynotes.dialogs

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.riard.mynotes.R
import com.riard.mynotes.adapters.FolderAdapter
import com.riard.mynotes.events.EventSelectedFolder
import com.riard.mynotes.mvp.common.Preferences
import org.greenrobot.eventbus.EventBus

class FoldersListDialog : BaseDialog(), FolderAdapter.FolderDialogDelegate {

    private lateinit var pref: Preferences

    override fun onCreateDialog(savedInstanceState: Bundle?)
            = super.onCreateDialog(savedInstanceState).apply {
            try {
                window!!.requestFeature(Window.FEATURE_NO_TITLE)
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_add_folder, container, false)
        pref = Preferences(context!!)
        var folders = pref.getFoldersList()
        if (folders == null) {
            folders = ArrayList()
        }
        folders.run {
            if (size != 0) {
                add(0, context!!.getString(R.string.without_folder))
            }
            val rvFoldersList = view.findViewById<RecyclerView>(R.id.rvFoldersList)
            val tvEmptyFolderListWarning = view.findViewById<TextView>(R.id.tvEmptyFolderListWarning)
            val btnCreateNewFolder = view.findViewById<Button>(R.id.btnCreateNewFolder)
            btnCreateNewFolder.setTextColor(pref.getColorThemeId())
            btnCreateNewFolder.setOnClickListener {
                AlertDialog.Builder(context!!).apply {
                    setTitle(context!!.resources.getString(R.string.enter_name_folder))
                    val view = inflater.inflate(R.layout.item_folder_edit, container, false)
                    val etNewFolderName = view.findViewById<EditText>(R.id.etNewFolderName)
                    setView(view)
                    setPositiveButton(context!!.resources.getString(R.string.confirm)) { dialog, _ ->
                        if (etNewFolderName.text.trim().isNotEmpty() && !contains(etNewFolderName.text.trim())) {
                            add(etNewFolderName.text.trim().toString())
                            if (size > 1) {
                                removeAt(0)
                            }
                            pref.setFoldersList(this@run)
                            add(0, context!!.getString(R.string.without_folder))
                            if (size > 2) {
                                rvFoldersList.adapter!!.notifyDataSetChanged()
                            } else {
                                rvFoldersList.visibility = View.VISIBLE
                                tvEmptyFolderListWarning.visibility = View.INVISIBLE
                                rvFoldersList.layoutManager = LinearLayoutManager(context!!)
                                rvFoldersList.adapter = FolderAdapter(this@run, this@FoldersListDialog)
                            }
                        } else {
                            Toast.makeText(context!!, "Name can\'t be empty!", Toast.LENGTH_SHORT).show()
                        }
                        dialog.dismiss()
                    }
                    setNegativeButton(context!!.resources.getString(R.string.cancel)) { dialog, which ->
                        dialog.dismiss()
                    }
                    create().show()
                }
            }
            if (isEmpty()) {
                rvFoldersList.visibility = View.INVISIBLE
                tvEmptyFolderListWarning.visibility = View.VISIBLE
            } else {
                rvFoldersList.apply {
                    layoutManager = LinearLayoutManager(context!!)
                    adapter = FolderAdapter(this@run, this@FoldersListDialog)
                }
            }
        }
        return view
    }

    override fun selectDialog(folderName: String) {
        dismiss()
        if (folderName == context!!.resources.getString(R.string.without_folder)) {
            EventBus.getDefault().post(EventSelectedFolder(""))
            return
        }
        EventBus.getDefault().post(EventSelectedFolder(folderName))
    }

    override fun deleteFolderByPosition(position: Int) {

    }
}

