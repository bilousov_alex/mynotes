package com.riard.mynotes.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.riard.mynotes.R
import com.riard.mynotes.adapters.ColorAdapter
import com.riard.mynotes.events.EventColorChange
import com.riard.mynotes.mvp.common.Preferences
import org.greenrobot.eventbus.EventBus

class DialogColor : BaseDialog(), ColorAdapter.ColorDialogDelegate {

    private val COLUMN_COUNT = 3
    private lateinit var pref: Preferences

    private val colorsArray = arrayListOf(R.drawable.ic_cycler_green, R.drawable.ic_cycler_blue,
            R.drawable.ic_cycler_red, R.drawable.ic_cycler_yellow, R.drawable.ic_cycler_violet,
            R.drawable.ic_cycler_pink, R.drawable.ic_cycler_orange, R.drawable.ic_cycler_black,
            R.drawable.ic_cycler_grey)

    override fun onCreateDialog(savedInstanceState: Bundle?)
            = super.onCreateDialog(savedInstanceState).apply {
            try {
                window!!.requestFeature(Window.FEATURE_NO_TITLE)
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.dialog_color, container, false).apply {
                pref = Preferences(context)
                var selectColor = pref.getColorThemeId()
                findViewById<RecyclerView>(R.id.rvColors).apply {
                    layoutManager = GridLayoutManager(context, COLUMN_COUNT)
                    adapter = ColorAdapter(colorsArray, selectColor, this@DialogColor)
                }
            }!!

    override fun selectDialog(position: Int) {
        pref.setColorThemeId(colorsArray[position])
        EventBus.getDefault().post(EventColorChange())
        dismiss()
    }

}