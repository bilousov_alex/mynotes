package com.riard.mynotes.dialogs

import androidx.fragment.app.DialogFragment

open class BaseDialog : DialogFragment()