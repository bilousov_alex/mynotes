package com.riard.mynotes.activities

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.riard.mynotes.App
import com.riard.mynotes.R
import com.riard.mynotes.mvp.common.Preferences
import com.riard.mynotes.mvp.common.Utility

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(getColorTheme(Preferences(baseContext).getColorThemeId()))
        super.onCreate(savedInstanceState)
        Utility.resetActivityTitle(this)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(App.localeManager!!.setLocale(newBase!!))
    }

    private fun getColorTheme(colorId: Int) = when (colorId) {
        R.drawable.ic_cycler_green -> {
            Preferences(baseContext).setFolderColorId(R.color.greenPrimary)
            R.style.AppTheme_Green
        }
        R.drawable.ic_cycler_blue -> {
            Preferences(baseContext).setFolderColorId(R.color.bluePrimary)
            R.style.AppTheme_Blue
        }
        R.drawable.ic_cycler_red -> {
            Preferences(baseContext).setFolderColorId(R.color.redPrimary)
            R.style.AppTheme_Red
        }
        R.drawable.ic_cycler_yellow -> {
            Preferences(baseContext).setFolderColorId(R.color.yellowPrimary)
            R.style.AppTheme_Yellow
        }
        R.drawable.ic_cycler_violet -> {
            Preferences(baseContext).setFolderColorId(R.color.violetPrimary)
            R.style.AppTheme_Violet
        }
        R.drawable.ic_cycler_pink -> {
            Preferences(baseContext).setFolderColorId(R.color.pinkPrimary)
            R.style.AppTheme_Pink
        }
        R.drawable.ic_cycler_orange -> {
            Preferences(baseContext).setFolderColorId(R.color.orangePrimary)
            R.style.AppTheme_Orange
        }
        R.drawable.ic_cycler_black -> {
            Preferences(baseContext).setFolderColorId(R.color.blackPrimary)
            R.style.AppTheme_Black
        }
        R.drawable.ic_cycler_grey -> {
            Preferences(baseContext).setFolderColorId(R.color.greyPrimary)
            R.style.AppTheme_Grey
        }
        else -> {
            Preferences(baseContext).setFolderColorId(R.color.bluePrimary)
            R.style.AppTheme_Blue
        }
    }
}