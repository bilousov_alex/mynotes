package com.riard.mynotes.activities

import android.os.Bundle
import com.riard.mynotes.dialogs.DialogColor
import com.riard.mynotes.events.EventColorChange
import com.riard.mynotes.mvp.common.Preferences
import kotlinx.android.synthetic.main.activity_settings.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import android.content.Intent
import android.net.Uri
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.riard.mynotes.App
import com.riard.mynotes.BuildConfig
import com.riard.mynotes.R
import com.riard.mynotes.fragments.FoldersListSettingsFragment


class SettingsActivity : BaseActivity() {

    private var pref: Preferences? = null
    companion object {
        private var isUpdate = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar3)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        pref = Preferences(baseContext)

        setLanguage()

        versionApp.text = BuildConfig.VERSION_NAME

        ivSelectColor.setImageDrawable(resources.getDrawable(pref!!.getColorThemeId(), null))

        textChangeColor.setOnClickListener {
            DialogColor().show(supportFragmentManager, "")
        }

        selectLanguage.setOnClickListener {
            showSelectLanguageDialog()
        }

        shownSelectedLanguage.setOnClickListener {
            showSelectLanguageDialog()
        }

        rateMyApp.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$packageName")))
            } catch (e: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
            }

        }
        textFolders.setOnClickListener {
            frameContent.visibility = View.VISIBLE
            supportFragmentManager.beginTransaction().addToBackStack(null)
                    .setCustomAnimations(R.anim.slide_from_left, R.anim.slide_to_right)
                    .replace(R.id.frameContent, FoldersListSettingsFragment()).commit()
        }
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onStop() {
        super.onStop()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.fragments.isNotEmpty()) {
            supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left)
                    .remove(supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1])
                    .commit()
            return
        }
        if (isUpdate) {
            var intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
        isUpdate = false
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                if (supportFragmentManager.fragments.isNotEmpty()) {
                    supportFragmentManager
                            .beginTransaction()
                            .setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left)
                            .remove(supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1])
                            .commit()
                    return true
                }
                if (isUpdate) {
                    var intent = Intent(this, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                isUpdate = false
                finish()
            }
        }
        return true
    }

    @Subscribe
    fun changeColor(event: EventColorChange) {
        ivSelectColor.setImageDrawable(resources.getDrawable(pref!!.getColorThemeId(), null))
        recreate()
        isUpdate = true
    }

    private fun showSelectLanguageDialog() {
        val builder = AlertDialog.Builder(this)
        val array = resources.getStringArray(R.array.languages)
        val arrayCode = resources.getStringArray(R.array.code_country)
        builder.setItems(array) { dialog, position ->
            App.localeManager!!.setNewLocale(this, arrayCode[position])
            shownSelectedLanguage.text = array[position]
            recreate()
            isUpdate = true
            dialog!!.dismiss()
        }
        builder.create().show()
    }

    private fun setLanguage() {
        val array = resources.getStringArray(R.array.languages)
        val arrayCode = resources.getStringArray(R.array.code_country)
        var language = App.localeManager!!.getLanguage()
        var i = 0
        while (i < arrayCode.size) {
            if (arrayCode[i] == language) {
                shownSelectedLanguage.text = array[i]
                return
            }
            i++
        }
    }

}