package com.riard.mynotes.activities

import android.os.Bundle
import com.riard.mynotes.Constants
import com.riard.mynotes.R
import com.riard.mynotes.events.EventBackPress
import com.riard.mynotes.fragments.NotesListFragment
import com.riard.mynotes.fragments.NoteFragment
import com.riard.mynotes.fragments.PreloadFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportFragmentManager.beginTransaction()
                .add(R.id.frameContent, PreloadFragment()).commit()


    }

    override fun onBackPressed() {
        supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1].run {
            var code = when (this) {
                is NotesListFragment -> {
                    finish()
                    Constants.NOTES_LIST_FRAGMENT
                }
                is NoteFragment -> {
                    Constants.NOTE_FRAGMENT
                }
                else -> {
                    -1
                }
            }
            EventBus.getDefault().post(EventBackPress(code))
        }
    }

}
