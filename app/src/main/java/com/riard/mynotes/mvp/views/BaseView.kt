package com.riard.mynotes.mvp.views

interface BaseView {
    fun showToastMessage(message: String)
}