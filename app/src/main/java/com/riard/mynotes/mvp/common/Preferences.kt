package com.riard.mynotes.mvp.common

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.riard.mynotes.Constants
import com.riard.mynotes.R

class Preferences(val context: Context) {

    private var preferences = context.getSharedPreferences(Constants.FILE_NAME, Context.MODE_PRIVATE)
    private var edit = preferences.edit()

    fun setColorThemeId(colorId: Int) {
        edit.putInt(Constants.COLOR_ID, colorId).apply()
    }

    fun setFolderColorId(colorId: Int){
        edit.putInt(Constants.FOLDER_COLOR_ID, colorId).apply()
    }


    fun setFoldersList(labels: ArrayList<String>) {
        edit.putString(Constants.FOLDER, Gson().toJson(labels)).apply()
    }

    fun setFilter(isLookAtFilter: Boolean) {
        edit.putBoolean(Constants.FILTER, isLookAtFilter).apply()
    }

    fun setCurrentFolder(folder: String) {
        edit.putString(Constants.CURRENT_FOLDER, folder).apply()
    }

    fun getColorThemeId() = preferences.getInt(Constants.COLOR_ID, R.drawable.ic_cycler_blue)

    fun getFolderColorId() = preferences.getInt(Constants.FOLDER_COLOR_ID, R.color.blue)

    fun getFoldersList() : ArrayList<String>? = Gson().fromJson(
            preferences.getString(Constants.FOLDER, null), object : TypeToken<ArrayList<String>?>() {}.type )

    fun getFilter() = preferences.getBoolean(Constants.FILTER, true)

    fun getCurrentFolder() = preferences.getString(Constants.CURRENT_FOLDER, " ")!!


}