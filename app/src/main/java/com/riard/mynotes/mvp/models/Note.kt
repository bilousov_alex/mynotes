package com.riard.mynotes.mvp.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notes")
data class Note(@PrimaryKey(autoGenerate = true) var id: Long? = null, var text: String,
                var updateDate: Long, var createDate: Long? = null, var isSelected: Boolean = false,
                var stringDate: String? = null, var title: String? = null, var shortText: String? = null,
                var label: String = "") {

    init {
        if (createDate == null) {
            createDate = updateDate
        }
    }
}