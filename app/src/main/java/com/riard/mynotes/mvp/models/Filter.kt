package com.riard.mynotes.mvp.models

data class Filter(var isSwitchOn: Boolean, var folder: String)