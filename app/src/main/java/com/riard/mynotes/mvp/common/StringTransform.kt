package com.riard.mynotes.mvp.common

import java.text.SimpleDateFormat
import java.util.*

class StringTransform {

    companion object {

        fun convertLongToTime(time: Long) = Date(time).run {
                var format = if (year == Date(System.currentTimeMillis()).year) {
                    SimpleDateFormat("MMM d HH:mm")
                } else {
                    SimpleDateFormat("MMM d, yyyy HH:mm")
                }
                return@run format.format(this)
            }

        fun getNoteTitle(text: String) = text.split("\n")[0]

        fun getNoteDescriptionShort(text: String) = (text.replace(text.split("\n")[0], " ")).trim()

    }
}