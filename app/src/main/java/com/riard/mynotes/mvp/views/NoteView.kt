package com.riard.mynotes.mvp.views

interface NoteView : BaseView {
    fun showNoteDate(date: String)
    fun showCharactersCount(countCharacters: String)
    fun showNoteText(textNote: String)
    fun openKeyboard()
    fun closeScreen()
    fun showNoteTitle(title: String)
}