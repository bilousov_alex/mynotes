package com.riard.mynotes.mvp.views

interface FoldersListSettingsView : BaseView {
    fun showFoldersList(folders: ArrayList<String>)
    fun showFoldersListIsEmptyWarning()
    fun deleteFolderByPositionFromUI(position: Int)
    fun insertFolderToEnd(position: Int)
}