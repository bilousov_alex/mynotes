package com.riard.mynotes.mvp.presenters

import android.annotation.SuppressLint
import android.util.Log
import com.riard.mynotes.Constants
import com.riard.mynotes.R
import com.riard.mynotes.mvp.models.Filter
import com.riard.mynotes.mvp.models.Note
import com.riard.mynotes.mvp.views.NotesListView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlin.collections.ArrayList

class NotesListPresenter(private val notesListView: NotesListView) : BasePresenter(notesListView) {

    private var searchedBefore = false
    private var oldSearchText = ""
    private var selectNotePosition = -1

    private var notes = ArrayList<Note>()
    private var filter: Filter? = null
    private var isSelectMode = false

    fun initDB() {
        filter = Filter(false, " ")
    }

    fun loadNotesList() {
        filter?.let {
            it.folder = pref.getCurrentFolder()
            val folderList = pref.getFoldersList()
            if (folderList == null) {
                notesListView.showCurrentFolder(context.resources.getString(R.string.all_folder))
                dao.getAllNotes()
                return@let
            }
            folderList.let { array ->
                if (!array.contains(it.folder) || it.folder == " ") {
                    notesListView.showCurrentFolder(context.resources.getString(R.string.all_folder))
                    dao.getAllNotes()
                } else {
                    notesListView.showCurrentFolder(it.folder)
                    dao.getAllNotesByFolderName(it.folder)
                }
            }
        }
    }

    fun createNewNote() {
        filter?.let {
            if (it.folder == " " || it.folder == context!!.resources.getString(R.string.all_folder)) {
                notesListView.openNoteCreateScreen(" ")
            } else {
                notesListView.openNoteCreateScreen(it.folder)
            }
        }
    }

    fun initFoldersListDialog(notePosition: Int) {
        selectNotePosition = notePosition
        notesListView.showFoldersListDialog()
    }

    fun searchNotesByText(searchText: String) {
        searchText.run {
            if ((this.trim().isEmpty() && searchedBefore) || oldSearchText.length > length) {
                loadNotesList()
                searchedBefore = false
                oldSearchText = this
                return
            }
            oldSearchText = this
            var i = 0
            while (i != notes.size) {
                if (!notes[i].text.toLowerCase().contains(toLowerCase())) {
                    notes.removeAt(i)
                    searchedBefore = true
                    continue
                }
                i++
            }
            if (searchedBefore) {
                notesListView.updateNotesList()
                searchEmptyNotes()
            }
        }
    }

    fun editNoteByPosition(position: Int) {
        notesListView.openNoteEditScreenById(notes[position].id!!)
    }

    override fun sendNotesList(notes: ArrayList<Note>) {
        this.notes.run {
            if (isNotEmpty()) {
                clear()
            }
            addAll(notes)
            sortByDescending { it.updateDate }
            notesListView.showNotesList(this)
        }
        searchEmptyNotes()
    }

    private fun searchEmptyNotes() {
        if (this.notes.isEmpty()) {
            if (filter!!.folder == " " || filter!!.folder == context!!.resources.getString(R.string.all_folder)) {
                notesListView.showNotesListIsEmptyWarning()
            } else {
                notesListView.showNotesListIsEmptyInCurrentFolderWarning()
            }
        }
    }

    override fun updateNote() {
        notesListView.updateNoteByPosition(selectNotePosition)
        filter?.let {
            if (selectNotePosition != -1
                    && notes[selectNotePosition].label != it.folder
                    && it.folder != " "
                    && it.folder != context!!.resources.getString(R.string.all_folder)) {
                notes.removeAt(selectNotePosition)
                notesListView.deleteNoteByPosition(selectNotePosition)
            }
            searchEmptyNotes()
            selectNotePosition = -1
        }
    }

    fun filterAction() {
        filter!!.isSwitchOn = if (!pref.getFilter()) {
            notesListView.hideFilter()
            false
        } else {
            notesListView.showFilter()
            true
        }
    }

    fun turnOffFilter() {
        pref.setFilter(!pref.getFilter())
        filterAction()
    }

    fun setFilter() {
        var folder = pref.getFoldersList()
        var charFolder = ArrayList<CharSequence>()
        if (folder == null) {
            folder = ArrayList()
        }
        folder.add(0, context!!.resources.getString(R.string.all_folder))
        Observable.fromIterable(folder)
                .subscribe {
                    charFolder.add(it)
                }
        notesListView.setFilter(charFolder)
    }

    fun filterNotesListBySelectedFolder(folder: String) {
        filter!!.folder = folder
        if (folder == context!!.resources.getString(R.string.all_folder)) {
            pref.setCurrentFolder(" ")
            dao.getAllNotes()
        } else {
            pref.setCurrentFolder(folder)
            dao.getAllNotesByFolderName(folder)
        }
    }

    fun addSelectedFolderToNote(folderName: String) {
        if (!isSelectMode) {
            notes[selectNotePosition].label = folderName
            dao.updateFolderById(notes[selectNotePosition].id!!, folderName)
        } else {
            addFolderToSelectNotes(folderName)
        }
    }

    fun action(actionType: Int, countSelectedNotes: Int) {
        notesListView.run {
            when (actionType) {
                Constants.SELECT_MODE_ON -> {
                    isSelectMode = true
                    showSelectMode()
                }
                Constants.SELECT_MODE_OFF -> {
                    isSelectMode = false
                    showNormalMode()
                }
                Constants.SELECTED_NOTES_COUNT -> {
                    if (countSelectedNotes == 0) {
                        showNormalMode()
                    } else {
                        changeSelectedNotesCount(countSelectedNotes.toString())
                    }
                }
            }
        }
    }

    fun switchOffSelectedMode() {
        for(note in notes) {
            note.isSelected = false
        }
        notesListView.run {
            switchOffSelectModeInNotesList()
            updateNotesList()
        }
        searchEmptyNotes()
    }

    @SuppressLint("CheckResult")
    fun addFolderToSelectNotes(folderName: String) {
        Observable.fromIterable(notes)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter { note -> note.isSelected }
                .flatMap { note ->
                    dao.updateFolderById(note.id!!, folderName)
                    Observable.just(note)
                }.subscribe({

                }, {

                }, {
                    isSelectMode = false
                    notesListView.run {
                        showNormalMode()
                        switchOffSelectModeInNotesList()
                    }
                    loadNotesList()
                })
    }

    @SuppressLint("CheckResult")
    fun deleteSelectedNotes() {
        Observable.fromIterable(notes)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter { note -> note.isSelected }
                .flatMap { note ->
                    dao.deleteNote(note)
                    notes.remove(note)
                    Observable.just(note.id)
                }
                .subscribe ({
                    Log.d("ID NOTE ", "id = " + it!!)
                }, {

                }, {
                    isSelectMode = false
                    notesListView.run {
                        showNormalMode()
                        switchOffSelectModeInNotesList()
                        updateNotesList()
                    }
                    searchEmptyNotes()
                })
    }

    fun initSelectedNotesDeleteDialog() {
        notesListView.showSelectedNotesDeleteDialog()
    }

    fun initFolderAddToSelectedNotesDialog() {
        notesListView.showFoldersListDialog()
    }

}
