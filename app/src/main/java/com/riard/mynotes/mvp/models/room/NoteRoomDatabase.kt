package com.riard.worldtest.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.riard.mynotes.mvp.models.Note
import com.riard.mynotes.mvp.models.room.Migration1To2


@Database(entities = [(Note::class)], version = 2)
@TypeConverters(Converter::class)
abstract class NoteRoomDatabase : RoomDatabase() {

    abstract fun noteDao() : NoteDao

    companion object {
        private var INSTANCE: NoteRoomDatabase? = null

        fun getInstance(context: Context): NoteRoomDatabase? {
            if (INSTANCE == null) {
                synchronized(NoteRoomDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            NoteRoomDatabase::class.java, "notes.db")
                            .addMigrations(migrate1To2)
                            .build()
                }
            }
            return INSTANCE
        }

        val migrate1To2 = Migration1To2()
    }



}