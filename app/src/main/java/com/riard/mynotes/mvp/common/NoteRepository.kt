package com.riard.mynotes.mvp.common

import com.riard.mynotes.mvp.models.Note

interface NoteRepository {
    fun insertNote(note: Note)
    fun updateNote(note: Note)
    fun updateFolderById(idNote: Long, folder: String)
    fun deleteFolderFromNoteByNoteId(id: Long)
    fun deleteNote(note: Note)
    fun getAllNotes()
    fun getAllNotesByFolderName(folder: String)
    fun getNoteById(id: Long)
    fun getNoteByTime(time: Long)
}