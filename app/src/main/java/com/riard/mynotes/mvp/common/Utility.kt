package com.riard.mynotes.mvp.common

import android.os.Build
import android.app.Activity
import android.content.pm.PackageManager.GET_META_DATA
import android.content.pm.PackageManager
import android.content.res.Resources
import java.lang.ref.WeakReference


class Utility {
    companion object {

        fun getPrivateField(className: String, fieldName: String, objects: Any?): Any? {
            try {
                val c = Class.forName(className)
                val f = c.getDeclaredField(fieldName).apply {
                    isAccessible = true
                    return get(objects)
                }
            } catch (e: Throwable) {
                e.printStackTrace()
                return null
            }

        }

        fun resetActivityTitle(a: Activity) {
            try {
                val info = a.packageManager.getActivityInfo(a.componentName, GET_META_DATA)
                if (info.labelRes != 0) {
                    a.setTitle(info.labelRes)
                }
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

        }

        fun isAtLeastVersion(version: Int): Boolean {
            return Build.VERSION.SDK_INT >= version
        }
    }
}