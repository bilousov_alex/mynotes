package com.riard.mynotes.mvp.common

import android.content.Context
import com.riard.mynotes.mvp.models.Note
import com.riard.worldtest.room.NoteRoomDatabase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DBNoteRepository(context: Context, delegate: NoteServiceDelegate?) : NoteRepository {

    private var noteDao = NoteRoomDatabase.getInstance(context)!!.noteDao()
    private var delegate = delegate

    interface NoteServiceDelegate {
        fun sendNotesList(notes: ArrayList<Note>)
        fun sendNoteById(note: Note)
        fun sendNewNoteId(id:Long)
        fun noteWasDeleted()
        fun updateNote()
    }

    override fun insertNote(note: Note) {
        Completable.fromAction {
            noteDao.insert(note)
            getNoteByTime(note.createDate!!)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    override fun updateNote(note: Note) {
        Completable.fromAction {
            noteDao.update(note)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    override fun updateFolderById(noteId: Long, folderName: String) {
        Completable.fromAction {
            noteDao.updateFolderById(noteId, folderName)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    delegate!!.updateNote()
                }
    }

    override fun deleteFolderFromNoteByNoteId(noteId: Long) {
        Completable.fromAction {
            noteDao.deleteFolderFromNotesByNoteId(noteId)
        }.subscribeOn(Schedulers.io())
                .subscribe()
    }

    override fun deleteNote(note: Note) {
        Completable.fromAction {
            noteDao.delete(note)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    delegate!!.noteWasDeleted()
                }
    }

    override fun getAllNotes() {
        noteDao.getAllNotes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { notes ->
                    delegate!!.sendNotesList(ArrayList(notes))
                }
    }

    override fun getAllNotesByFolderName(folderNote: String) {
        noteDao.getNoteByFolder(folderNote)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { notes ->
                    delegate!!.sendNotesList(ArrayList(notes))
                }
    }

    override fun getNoteById(noteId: Long) {
        noteDao.getNoteById(noteId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { note ->
                    delegate!!.sendNoteById(note)
                }
    }

    override fun getNoteByTime(time: Long) {
        noteDao.getNoteByTime(time)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { note ->
                    delegate!!.sendNewNoteId(note.id!!)
                }
    }
}