package com.riard.worldtest.room

import androidx.room.*
import com.riard.mynotes.mvp.models.Note
import io.reactivex.Maybe


@Dao
interface NoteDao {

    @Insert
    fun insert(note: Note) : Long

    @Update
    fun update(note: Note)

    @Delete
    fun delete(vararg note: Note)

    @Query("select * from notes")
    fun getAllNotes() : Maybe<List<Note>>

    @Query("select * from notes where label = :folder")
    fun getNoteByFolder(folder: String) : Maybe<List<Note>>

    @Query("select * from notes where id = :idNote")
    fun getNoteById(idNote: Long) : Maybe<Note>

    @Query("select * from notes where createDate = :time")
    fun getNoteByTime(time: Long) : Maybe<Note>

    @Query("update notes set label = :folder where id = :idNote")
    fun updateFolderById(idNote: Long, folder: String)

    @Query("update notes set label = \"\" where id = :id")
    fun deleteFolderFromNotesByNoteId(id: Long)
}