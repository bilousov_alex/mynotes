package com.riard.mynotes.mvp.views

import com.riard.mynotes.mvp.models.Note

interface NotesListView : BaseView {
    fun showNotesList(notes: ArrayList<Note>)
    fun updateNotesList()
    fun updateNoteByPosition(position: Int)
    fun deleteNoteByPosition(position: Int)
    fun openNoteCreateScreen(folder: String)
    fun openNoteEditScreenById(id: Long)
    fun hideFilter()
    fun showFilter()
    fun setFilter(folder: ArrayList<CharSequence>)
    fun showFoldersListDialog()
    fun showCurrentFolder(folder: String)
    fun showSelectMode()
    fun showNormalMode()
    fun changeSelectedNotesCount(countSelectedNotes: String)
    fun switchOffSelectModeInNotesList()
    fun showSelectedNotesDeleteDialog()
    fun showNotesListIsEmptyWarning()
    fun showNotesListIsEmptyInCurrentFolderWarning()
}