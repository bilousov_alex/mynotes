package com.riard.mynotes.mvp.presenters

import android.app.AlertDialog
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.Toast
import com.riard.mynotes.R
import com.riard.mynotes.mvp.models.Note
import com.riard.mynotes.mvp.views.FoldersListSettingsView
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlin.collections.ArrayList

class FoldersListSettingsPresenter(private val foldersListSettingsView: FoldersListSettingsView) : BasePresenter(foldersListSettingsView) {

    private var folders: ArrayList<String>? = null

    fun loadFoldersList() {
        folders = pref.getFoldersList()?.let {
            it.run {
                if (isEmpty()) {
                    foldersListSettingsView.showFoldersListIsEmptyWarning()
                    return@let null
                }
                foldersListSettingsView.showFoldersList(this)
                return@let this
            }
        }

    }

    fun initFolderCreateDialog() {
        AlertDialog.Builder(context).apply {
            setTitle(context!!.resources.getString(R.string.enter_name_folder))
            val view = LayoutInflater.from(context).inflate(R.layout.item_folder_edit, null, false)
            val etFolderName = view.findViewById<EditText>(R.id.etNewFolderName)
            setView(view)
            setPositiveButton(context!!.resources.getString(R.string.confirm)) { dialog, _ ->
                folders?.let {
                    it.run {
                        if (etFolderName.text.trim().isNotEmpty() && !contains(etFolderName.text.trim())) {
                            add(etFolderName.text.trim().toString())
                            pref.setFoldersList(this)
                            if (size > 0) {
                                foldersListSettingsView.showFoldersList(this)
                            } else {
                                foldersListSettingsView.insertFolderToEnd(size - 1)
                            }
                        } else {
                            Toast.makeText(context!!, "Name can\'t be empty!", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                dialog.dismiss()
            }
            setNegativeButton(context!!.resources.getString(R.string.cancel)) { dialog, _ ->
                dialog.dismiss()
            }
            create().show()
        }
    }

    override fun sendNotesList(notes: ArrayList<Note>) {
        super.sendNotesList(notes)
        Observable.fromIterable(notes)
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    dao.deleteFolderFromNoteByNoteId(it.id!!)
                }
    }

    fun deleteFolderByPosition(position: Int) {
        folders?.let {
            it.run {
                dao.getAllNotesByFolderName(this[position])
                removeAt(position)
                pref.setFoldersList(this)
                foldersListSettingsView.run {
                    deleteFolderByPositionFromUI(position)
                    if (isEmpty()) {
                        showFoldersListIsEmptyWarning()
                    }
                }
            }
        }

    }
}