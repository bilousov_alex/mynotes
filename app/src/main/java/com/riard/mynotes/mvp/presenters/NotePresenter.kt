package com.riard.mynotes.mvp.presenters

import androidx.appcompat.app.AlertDialog
import com.riard.mynotes.R
import com.riard.mynotes.mvp.common.StringTransform
import com.riard.mynotes.mvp.models.Note
import com.riard.mynotes.mvp.views.NoteView

class NotePresenter(private val noteView: NoteView) : BasePresenter(noteView) {

    private var note: Note? = null
    private var updateNote = false
    private var updateTitle = false
    private var oldText = ""
    private var oldTitle = ""
    private lateinit var folderNameForNote: String

    fun loadInfoById(id: Long) {
        dao.getNoteById(id)
    }

    fun updateCountCharacters(textNote: String) {
        updateNote = true
        setCountCharacters(textNote)
    }

    private fun setCountCharacters(textNote: String) {
        note?.let {
            it.run {
                text = textNote
                updateDate = System.currentTimeMillis()
                noteView.run {
                    if (text.length == 1) {
                        showCharactersCount(context!!.resources.getString(R.string.note_one_character))
                        return
                    }
                    showCharactersCount(String.format(context!!.resources.getString(R.string.note_count_characters),
                            text.length))
                }
            }
        }
    }

    fun closeScreen() {
        note?.let {
            it.run {
                if ((updateNote && oldText.isEmpty() && text.isNotEmpty()) ||
                        (updateNote && oldText.isNotEmpty() && text.trim() != oldText)
                        || updateTitle && oldTitle.isEmpty() && title!!.isNotEmpty()
                        || updateTitle && oldTitle.isNotEmpty() && text.trim() != oldTitle) {
                    AlertDialog.Builder(context).apply {
                        setTitle(context!!.resources.getString(R.string.dialog_close_title))
                        setNegativeButton(context!!.resources.getString(R.string.dialog_no)) { _, _ ->
                            if (text.trim().isEmpty()) {
                                dao.deleteNote(this@run)
                            }
                            noteView.closeScreen()
                        }
                        setPositiveButton(context!!.resources.getString(R.string.dialog_yes)) { _, _ ->
                            if (text.trim().isEmpty() && (title != null && title!!.trim().isEmpty())) {
                                dao.deleteNote(this@run)
                            } else {
                                text = text.trim()
                                dao.updateNote(this@run)
                                noteView.showToastMessage(context!!.resources.getString(R.string.note_saved))
                            }
                            noteView.closeScreen()
                        }
                        create().show()
                    }
                    return
                }
                if (text.trim().isEmpty()) {
                    dao.deleteNote(this)
                    return
                }
                noteView.closeScreen()
            }
        }
    }

    fun deleteNote() {
        note?.let {
            it.run {
                if (text.trim().isEmpty() && ((title != null && title!!.isEmpty())
                                || title == null)) {
                    dao.deleteNote(this)
                } else {
                    AlertDialog.Builder(context).apply {
                        setTitle(context!!.resources.getString(R.string.dialog_delete_title))
                        setNegativeButton(context!!.resources.getString(R.string.dialog_no)) { dialog, _ ->
                            dialog.dismiss()
                        }
                        setPositiveButton(context!!.resources.getString(R.string.dialog_yes)) { _, _ ->
                            dao.deleteNote(this@run)
                            noteView.showToastMessage(context!!.resources.getString(R.string.note_deleted))
                        }
                        create().show()
                    }
                }
            }
        }
    }

    fun saveNote() {
        note?.let {
            it.run {
                if (text.trim().isEmpty() || (title != null && title!!.trim().isNotEmpty())) {
                    text = text.trim()
                    title?.let {
                        this.title = it.trim()

                    }
                    dao.updateNote(this)
                    updateNote = false
                    updateTitle = false
                    noteView.showToastMessage(context!!.resources.getString(R.string.note_saved))
                }
            }
        }
    }

    fun createNote() {
        note = Note(text = "", updateDate = System.currentTimeMillis())
        note?.let {
            it.run {
                setCountCharacters("")
                if (folderNameForNote.trim().isNotEmpty()) {
                    label = folderNameForNote
                }
                dao.insertNote(this)
                noteView.showNoteDate(StringTransform.convertLongToTime(updateDate))
            }
        }
    }

    override fun sendNoteById(note: Note) {
        note.run {
            this@NotePresenter.note = this
            oldText = text
            title?.let {
                oldTitle = note.title!!
                noteView.showNoteTitle(this@NotePresenter.note!!.title!!)
            }
            noteView.showNoteText(this@NotePresenter.note!!.text)
            noteView.showNoteDate(StringTransform.convertLongToTime(updateDate))
            if (text.length == 1) {
                noteView.showCharactersCount(context!!.resources.getString(R.string.note_one_character))
                return
            }
            noteView.showCharactersCount(String.format(context!!.resources.getString(R.string.note_count_characters),
                    text.length))
        }
    }

    override fun sendNewNoteId(id: Long) {
        note!!.id = id
    }

    override fun noteWasDeleted() {
        noteView.closeScreen()
    }

    fun openKeyboard(isOpenKeyboard: Boolean) {
        if (isOpenKeyboard) {
            noteView.openKeyboard()
        }
    }

    fun changeTitle(noteTitle: String) {
        updateTitle = true
        note!!.title = noteTitle
    }

    fun setFolder(folderName: String?) {
        folderNameForNote = folderName!!
    }

}