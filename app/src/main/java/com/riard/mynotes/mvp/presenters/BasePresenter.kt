package com.riard.mynotes.mvp.presenters

import android.content.Context
import com.riard.mynotes.mvp.common.DBNoteRepository
import com.riard.mynotes.mvp.common.Preferences
import com.riard.mynotes.mvp.models.Note
import com.riard.mynotes.mvp.views.BaseView

open class BasePresenter(private val view: BaseView) : DBNoteRepository.NoteServiceDelegate {

    protected lateinit var context: Context
    protected lateinit var pref: Preferences
    protected lateinit var dao: DBNoteRepository

    fun setBaseContext(context: Context) {
        this.context = context
        pref = Preferences(context)
        dao = DBNoteRepository(context, this)
    }

    override fun sendNotesList(notes: ArrayList<Note>) {

    }

    override fun sendNoteById(note: Note) {

    }

    override fun sendNewNoteId(id: Long) {

    }

    override fun noteWasDeleted() {

    }

    override fun updateNote() {

    }
}