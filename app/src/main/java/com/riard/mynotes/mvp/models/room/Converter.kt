package com.riard.worldtest.room

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.riard.mynotes.mvp.models.Note


class Converter {

    @TypeConverter
    fun toNotesList(listNotes: String) : List<Note> = Gson().fromJson(listNotes,
            object : TypeToken<List<Note>>() {}.type)

    @TypeConverter
    fun toNote(note: String) : Note = Gson().fromJson(note,
            object : TypeToken<Note>() {}.type)

    @TypeConverter
    fun fromNotesList(listNotes: List<Note>) : String = Gson().toJson(listNotes)

    @TypeConverter
    fun fromNote(note: Note) : String = Gson().toJson(note)

}