package com.riard.mynotes.mvp.common

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Build.VERSION_CODES.JELLY_BEAN_MR1
import android.preference.PreferenceManager
import com.riard.mynotes.Constants.Companion.LANGUAGE_KEY
import java.util.*


class LocaleManager(val context: Context) {

    private var prefs: SharedPreferences? = null

    init {
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun setLocale(c: Context): Context {
        return updateResources(c, getLanguage())
    }

    fun setNewLocale(c: Context, language: String): Context {
        persistLanguage(language)
        return updateResources(c, language)
    }

    fun getLanguage(): String {
        return prefs!!.getString(LANGUAGE_KEY, context.resources.configuration.locale.language)!!
    }

    @SuppressLint("ApplySharedPref")
    private fun persistLanguage(language: String) {
        prefs!!.edit().putString(LANGUAGE_KEY, language).commit()
    }

    private fun updateResources(context: Context, language: String)= context.apply {
            Locale(language).run {
                Locale.setDefault(this)
                resources.let {
                    val config = Configuration(it.configuration)
                    if (Utility.isAtLeastVersion(JELLY_BEAN_MR1)) {
                        config.setLocale(this)
                        return createConfigurationContext(config)
                    } else {
                        config.locale = this
                        it.updateConfiguration(config, it.displayMetrics)
                    }
                }

            }
        }
}