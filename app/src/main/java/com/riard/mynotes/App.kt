package com.riard.mynotes

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import com.riard.mynotes.mvp.common.LocaleManager



class App : Application() {

    companion object {
        var localeManager: LocaleManager? = null
    }

    override fun attachBaseContext(base: Context) {
        localeManager = LocaleManager(base).apply {
            super.attachBaseContext(setLocale(base))
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localeManager!!.setLocale(this)
    }
}