package com.riard.mynotes.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.riard.mynotes.Constants
import com.riard.mynotes.R
import com.riard.mynotes.events.EventAction
import com.riard.mynotes.events.EventFolderAction
import com.riard.mynotes.events.EventOpenNote
import com.riard.mynotes.mvp.common.StringTransform
import com.riard.mynotes.mvp.models.Note
import kotlinx.android.synthetic.main.item_note.view.*
import org.greenrobot.eventbus.EventBus

class NoteAdapter(var notes: ArrayList<Note>) : RecyclerView.Adapter<NoteAdapter.ViewHolder>() {

    var selectMode = false
    private var selectedNotesCount = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_note,
                parent, false))
    }

    override fun getItemCount() = notes.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.run {
            tvNoteDate.text = StringTransform.convertLongToTime(notes[position].updateDate)
            if (selectMode) {
                ivSelectNote.visibility = View.VISIBLE
                if (notes[position].isSelected) {
                    ivSelectNote.setImageDrawable(context.getDrawable(R.drawable.ic_select_note))
                } else {
                    ivSelectNote.setImageDrawable(context.getDrawable(R.drawable.ic_not_select_note))
                }
            } else {
                ivSelectNote.visibility = View.GONE
            }
            if (notes[position].label.isNotEmpty()) {
                tvNoteFolderName.text = notes[position].label
            } else {
                tvNoteFolderName.text = context.getString(R.string.note_label)
            }
            if (notes[position].title == null || (notes[position].title != null && notes[position].title!!.trim().isEmpty())) {
                tvNoteTitle.text = StringTransform.getNoteTitle(notes[position].text)
                tvShortNoteDescription.text = StringTransform.getNoteDescriptionShort(notes[position].text)
            } else {
                tvNoteTitle.text = notes[position].title
                tvShortNoteDescription.text = notes[position].text
            }
        }
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        init {
            itemView.run {
                setOnLongClickListener {
                    selectMode = !selectMode
                    notes[adapterPosition].isSelected = !notes[adapterPosition].isSelected
                    if (selectMode) {
                        selectedNotesCount = 1
                        EventBus.getDefault().post(EventAction(Constants.SELECT_MODE_ON))
                    } else {
                        selectedNotesCount = 0
                        EventBus.getDefault().post(EventAction(Constants.SELECT_MODE_OFF))
                        for (note in notes) {
                            note.isSelected = false
                        }
                    }
                    EventBus.getDefault().post(EventAction(Constants.SELECTED_NOTES_COUNT, selectedNotesCount))
                    notifyDataSetChanged()
                    return@setOnLongClickListener true
                }
                tvNoteFolderName.setOnClickListener {
                    if (!selectMode) {
                        EventBus.getDefault().post(EventFolderAction(adapterPosition))
                    } else {
                        selectNote()
                    }
                }
                tvShortNoteDescription.setOnClickListener {
                    onClickNote()
                }
                setOnClickListener {
                    onClickNote()
                }
            }
        }

        private fun onClickNote() {
            if (selectMode) {
                selectNote()
                return
            }
            openNote()
        }

        private fun openNote() {
            EventBus.getDefault().post(EventOpenNote(adapterPosition))
        }

        private fun selectNote() {
            notes[adapterPosition].isSelected = if (notes[adapterPosition].isSelected) {
                selectedNotesCount--
                false
            } else {
                selectedNotesCount++
                true
            }
            EventBus.getDefault().post(EventAction(Constants.SELECTED_NOTES_COUNT, selectedNotesCount))
            if (selectedNotesCount == 0) {
                selectMode = false
                for(note in notes) {
                    note.isSelected = false
                }
                notifyDataSetChanged()
                return
            }
            notifyItemChanged(adapterPosition)
        }
    }
}