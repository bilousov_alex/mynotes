package com.riard.mynotes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.riard.mynotes.R
import kotlinx.android.synthetic.main.item_color.view.*

class ColorAdapter(var colors: ArrayList<Int>, private val selectColorId: Int, val colorDialogDelegate: ColorDialogDelegate)
    : RecyclerView.Adapter<ColorAdapter.ViewHolder>() {

    private lateinit var context: Context

    interface ColorDialogDelegate {
        fun selectDialog(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_color,
                parent, false))
    }

    override fun getItemCount() = colors.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.run {
            if (colors[position] == selectColorId) {
                ivSelectColor.visibility = View.VISIBLE
            } else {
                ivSelectColor.visibility = View.INVISIBLE
            }
            ivItemColor.setImageDrawable(context.getDrawable(colors[position]))
        }
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        init {
            itemView.setOnClickListener {
                colorDialogDelegate.selectDialog(adapterPosition)
            }
        }
    }
}