package com.riard.mynotes.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.riard.mynotes.R
import kotlinx.android.synthetic.main.item_folder.view.*


class FolderAdapter(var folders: ArrayList<String>, val delegate: FolderDialogDelegate, var settings: Boolean = false)
    : RecyclerView.Adapter<FolderAdapter.ViewHolder>() {


    interface FolderDialogDelegate {
        fun selectDialog(folderName: String)
        fun deleteFolderByPosition(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_folder,
                parent, false))
    }

    override fun getItemCount() = folders.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.run {
            tvFolderName.text = folders[position]
            if (settings) {
                ivDeleteImage.visibility = View.VISIBLE
                vFolderDelete.visibility = View.VISIBLE
            } else {
                ivDeleteImage.visibility = View.INVISIBLE
                vFolderDelete.visibility = View.INVISIBLE
            }
        }
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        init {
            itemView.run {
                setOnClickListener {
                delegate.selectDialog(folders[adapterPosition])
            }
                vFolderDelete.setOnClickListener {
                    delegate.deleteFolderByPosition(adapterPosition)
                }
            }
        }
    }
}