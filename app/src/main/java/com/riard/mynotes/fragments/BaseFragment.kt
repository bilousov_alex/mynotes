package com.riard.mynotes.fragments

import android.widget.Toast
import androidx.fragment.app.Fragment
import com.riard.mynotes.mvp.views.BaseView

open class BaseFragment : Fragment(), BaseView {

    override fun showToastMessage(message: String) {
        Toast.makeText(context!!, message, Toast.LENGTH_SHORT).show()
    }

}