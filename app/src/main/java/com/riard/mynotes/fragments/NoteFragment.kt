package com.riard.mynotes.fragments

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import com.riard.mynotes.Constants
import com.riard.mynotes.R
import com.riard.mynotes.events.EventBackPress
import com.riard.mynotes.mvp.presenters.NotePresenter
import com.riard.mynotes.mvp.views.NoteView
import kotlinx.android.synthetic.main.fragment_note.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_FORCED


class NoteFragment : BaseFragment(), NoteView {

    private val presenter = NotePresenter(this)
    private var menuSave: MenuItem? = null
    private var menuRW: MenuItem? = null
    private var isRead = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_note, container,false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        presenter.run {
            setBaseContext(context!!)
            arguments?.let {
                if (it.getLong(Constants.ID, -1) != -1L) {
                    isRead = true
                    loadInfoById(it.getLong(Constants.ID, -1))
                    return
                }
                openKeyboard(it.getBoolean(Constants.OPEN_KEYBOARD, false))
                setFolder(it.getString(Constants.FOLDER_FOR_NEW_NOTE, ""))
            }
            createNote()
        }
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onResume() {
        super.onResume()
        if (isRead) {
            (activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).apply {
                hideSoftInputFromWindow(view!!.windowToken, 0)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
        hideKeyboard(activity)
    }

    fun hideKeyboard(act: Activity?) {
        if (act != null && act.currentFocus != null) {
            val inputMethodManager = act.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(act.currentFocus!!.windowToken, 0)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.let {
            inflater!!.inflate(R.menu.note_menu, it)
            menuSave = it.findItem(R.id.menuSave)
            menuRW = it.findItem(R.id.menuWriteRead)
        }
        modeWriteRead()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menuDelete -> {
                presenter.deleteNote()
            }
            R.id.menuSave -> {
                presenter.saveNote()
            }
            R.id.menuWriteRead -> {
                isRead = !isRead
                modeWriteRead()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun modeWriteRead() {
        menuRW?.let {
            if (isRead) {
                it.icon = context!!.resources.getDrawable(R.drawable.ic_read, null)
                editNote.showSoftInputOnFocus = false
                (activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).apply {
                    hideSoftInputFromWindow(view!!.windowToken, 0)
                }
            } else {
                it.icon = context!!.resources.getDrawable(R.drawable.ic_edit, null)
                editNote.showSoftInputOnFocus = true
            }
        }

    }

    override fun showNoteDate(date: String) {
        dateNote.text = date
        editTitle.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                presenter.changeTitle(s.toString())
            }

        })
        editNote.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                presenter.updateCountCharacters(s.toString())
            }

        })
    }

    override fun showCharactersCount(countCharacters: String) {
        this.countCharacters.text = countCharacters
    }

    override fun showNoteText(textNote: String) {
        editNote.setText(textNote)
    }

    override fun showNoteTitle(title: String) {
        editTitle.setText(title)
    }

    override fun openKeyboard() {
        editNote.requestFocus()
        (activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).apply {
            toggleSoftInput(SHOW_FORCED, 0)
        }
    }

    override fun closeScreen() {
        activity!!.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left)
                .replace(R.id.frameContent, NotesListFragment())
                .commit()
    }

    @Subscribe
    fun onEventDeleteEmptyNotes(event: EventBackPress) {
        if (event.code == Constants.NOTE_FRAGMENT) {
            presenter.closeScreen()
        }
    }
}