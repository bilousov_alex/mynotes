package com.riard.mynotes.fragments

import android.content.Intent
import android.os.Bundle
import android.view.*
import com.riard.mynotes.Constants
import com.riard.mynotes.R
import com.riard.mynotes.activities.SettingsActivity
import com.riard.mynotes.adapters.NoteAdapter
import com.riard.mynotes.dialogs.FoldersListDialog
import com.riard.mynotes.events.*
import com.riard.mynotes.mvp.models.Note
import com.riard.mynotes.mvp.presenters.NotesListPresenter
import com.riard.mynotes.mvp.views.NotesListView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_notes_list.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.robinhood.ticker.TickerView


class NotesListFragment : BaseFragment(), NotesListView {

    private val presenter = NotesListPresenter(this)

    private lateinit var filterMenuItem: MenuItem
    private lateinit var searchMenuItem: MenuItem
    private lateinit var settingsMenuItem: MenuItem
    private lateinit var addFolderMenuItem: MenuItem
    private lateinit var deleteNotesMenuItem: MenuItem
    private lateinit var selectNotesCount: TickerView

    private lateinit var adapter: NoteAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_notes_list, container,false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        activity!!.toolbar.visibility = View.VISIBLE
        presenter.run {
            setBaseContext(context!!)
            initDB()
            btnCreateNote.setOnClickListener {
                createNewNote()
            }
            selectedFolder.setOnClickListener {
                setFilter()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.loadNotesList()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onStop() {
        super.onStop()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.let {
            inflater!!.inflate(R.menu.main_menu, it)
            filterMenuItem = it.findItem(R.id.menuFilter)
            searchMenuItem = it.findItem(R.id.menuSearch)
            settingsMenuItem = it.findItem(R.id.menuSettings)
            addFolderMenuItem = it.findItem(R.id.menuAddFolder)
            deleteNotesMenuItem = it.findItem(R.id.menuDeleteNotes)
            presenter.filterAction()
            (it.findItem(R.id.menuSearch).actionView as SearchView).apply {
                setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextChange(newText: String): Boolean {
                        presenter.searchNotesByText(newText)
                        return true
                    }

                    override fun onQueryTextSubmit(query: String): Boolean {
                        return true
                    }
                })
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menuSettings -> {
                val intent = Intent(context!!, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.menuFilter -> {
                presenter.turnOffFilter()
            }
            R.id.menuAddFolder -> {
                presenter.initFolderAddToSelectedNotesDialog()
            }
            R.id.menuDeleteNotes -> {
                presenter.initSelectedNotesDeleteDialog()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showSelectMode() {
        settingsMenuItem.isVisible = false
        filterMenuItem.isVisible = false
        searchMenuItem.isVisible = false
        addFolderMenuItem.isVisible = true
        deleteNotesMenuItem.isVisible = true
        ((activity!! as AppCompatActivity).supportActionBar!!).apply {
            setDisplayShowTitleEnabled(false)
            setDisplayShowHomeEnabled(false)
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowCustomEnabled(true)
            val layoutParams = ActionBar.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                    ConstraintLayout.LayoutParams.MATCH_PARENT)
            LayoutInflater.from(context!!).inflate(R.layout.action_bar_select_mode, null).apply {
                setCustomView(this, layoutParams)
                val btnCancel = findViewById<ImageView>(R.id.selectModeCancel)
                selectNotesCount = findViewById(R.id.selectedCount)
                btnCancel.setOnClickListener {
                    presenter.switchOffSelectedMode()
                    showNormalMode()
                }
            }

        }

    }

    override fun showNotesListIsEmptyWarning() {
        tvNotesListIsEmptyWarning.run {
            visibility = View.VISIBLE
            text = context!!.resources.getText(R.string.not_notes)
        }
    }

    override fun showNotesListIsEmptyInCurrentFolderWarning() {
        tvNotesListIsEmptyWarning.run {
            visibility = View.VISIBLE
            text = context!!.resources.getText(R.string.no_notes_in_folder)
        }
    }

    override fun showNormalMode() {
        selectNotesCount.text = "1"
        settingsMenuItem.isVisible = true
        filterMenuItem.isVisible = true
        searchMenuItem.isVisible = true
        addFolderMenuItem.isVisible = false
        deleteNotesMenuItem.isVisible = false
        ((activity!! as AppCompatActivity).supportActionBar!!).apply {
            setDisplayShowTitleEnabled(true)
            setDisplayShowCustomEnabled(false)
        }
    }

    override fun switchOffSelectModeInNotesList() {
        adapter.selectMode = false

    }

    override fun showSelectedNotesDeleteDialog() {
        AlertDialog.Builder(context!!).apply {
            setTitle(context!!.resources.getString(R.string.dialog_delete_notes_title))
            setNegativeButton(context!!.resources.getString(R.string.dialog_no)) { dialog, _ ->
                dialog.dismiss()
            }
            setPositiveButton(context!!.resources.getString(R.string.dialog_yes)) { dialog, _ ->
                presenter.deleteSelectedNotes()
                dialog.dismiss()
            }
            create().show()
        }
    }

    override fun changeSelectedNotesCount(selectedNotesCount: String) {
        selectNotesCount.setText(selectedNotesCount, true)

    }

    override fun hideFilter() {
        filterMenuItem.icon = context!!.resources.getDrawable(R.drawable.ic_hide_filter, null)
        clFilter.visibility = View.GONE
    }

    override fun showFilter() {
        filterMenuItem!!.icon = context!!.resources.getDrawable(R.drawable.ic_show_filter, null)
        clFilter.visibility = View.VISIBLE
    }



    override fun setFilter(folder: ArrayList<CharSequence>) {
        AlertDialog.Builder(context!!).apply {
            setItems(folder.toTypedArray()) { _, position ->
                foldersFilter.setText(folder[position])
                presenter.filterNotesListBySelectedFolder(folder[position].toString())
            }
            create().show()
        }
    }

    override fun showNotesList(notes: ArrayList<Note>) {
        rvNotesList.apply {
            layoutManager = LinearLayoutManager(context!!)
            this@NotesListFragment.adapter = NoteAdapter(notes)
            adapter = this@NotesListFragment.adapter
            tvNotesListIsEmptyWarning.visibility = View.INVISIBLE
            addOnScrollListener(object : RecyclerView.OnScrollListener() {

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (dy > 0 || dy < 0 && btnCreateNote.isShown)
                        btnCreateNote.hide()
                }

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        btnCreateNote.show()
                    }
                    super.onScrollStateChanged(recyclerView, newState)
                }
            })
        }
    }

    override fun updateNotesList() {
        rvNotesList.adapter?.let {
            it.notifyDataSetChanged()
            tvNotesListIsEmptyWarning.visibility = View.VISIBLE
        }
    }

    override fun updateNoteByPosition(position: Int) {
        rvNotesList.adapter?.let {
            it.notifyItemChanged(position)
        }

    }

    override fun deleteNoteByPosition(position: Int) {
        rvNotesList.adapter?.let {
            it.notifyItemRemoved(position)
            tvNotesListIsEmptyWarning.visibility = View.VISIBLE
        }
    }

    override fun showFoldersListDialog() {
        FoldersListDialog().apply {
            show(this@NotesListFragment.activity!!.supportFragmentManager, "")
        }
    }

    override fun showCurrentFolder(folder: String) {
        foldersFilter.setText(folder)
    }

    override fun openNoteCreateScreen(folder: String) {
        NoteFragment().apply {
            val bundle = Bundle().apply {
                putBoolean(Constants.OPEN_KEYBOARD, true)
                putString(Constants.FOLDER_FOR_NEW_NOTE, folder)
            }
            arguments = bundle
            this@NotesListFragment.activity!!.supportFragmentManager
                    .beginTransaction()
                    .addToBackStack(null)
                    .setCustomAnimations(R.anim.slide_from_left, R.anim.slide_to_right)
                    .replace(R.id.frameContent, this)
                    .commit()
        }
    }

    override fun openNoteEditScreenById(id: Long) {
        NoteFragment().apply {
            val bundle = Bundle().apply {
                putLong(Constants.ID, id)
            }
            arguments = bundle
            this@NotesListFragment.activity!!.supportFragmentManager.beginTransaction().addToBackStack(null)
                    .setCustomAnimations(R.anim.slide_from_left, R.anim.slide_to_right)
                    .replace(R.id.frameContent, this).commit()
        }

    }

    @Subscribe
    fun noteAction(event: EventAction) {
        presenter.action(event.actionType, event.selectedNotesCount)
    }

    @Subscribe
    fun selectedFolderForNote(event: EventSelectedFolder) {
        presenter.addSelectedFolderToNote(event.folderName)
    }

    @Subscribe
    fun openNoteByPosition(event: EventOpenNote) {
        presenter.editNoteByPosition(event.id)
    }

    @Subscribe
    fun showFoldersListDialog(event: EventFolderAction) {
        presenter.initFoldersListDialog(event.adapterPosition)
    }

}