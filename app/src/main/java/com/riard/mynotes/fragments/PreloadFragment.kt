package com.riard.mynotes.fragments

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.riard.mynotes.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_preloader.*

class PreloadFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
        = inflater.inflate(R.layout.fragment_preloader, container, false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        activity!!.toolbar.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        val animationAlpha = ObjectAnimator.ofFloat(imageLogo, View.ALPHA, 0f, 1f, 1f)
        animationAlpha.duration = 2000
        val animationTextAlpha = ObjectAnimator.ofFloat(textLogo, View.ALPHA, 0f, 1f, 1f)
        animationTextAlpha.duration = 2000
        val animationTextAlphaShadow = ObjectAnimator.ofFloat(textLogoShadow, View.ALPHA, 0f, 1f, 1f)
        animationTextAlphaShadow.duration = 2000
        val animatorSet = AnimatorSet()
        animatorSet.play(animationAlpha).with(animationTextAlpha).with(animationTextAlphaShadow)
        animatorSet.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(p0: Animator?) {
                try {
                    activity!!.supportFragmentManager.beginTransaction().addToBackStack(null)
                            .replace(R.id.frameContent, NotesListFragment()).commit()
                } catch (e: Throwable) {

                }
            }

        })
        animatorSet.start()
    }


}