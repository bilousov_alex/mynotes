package com.riard.mynotes.fragments

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.riard.mynotes.R
import com.riard.mynotes.adapters.FolderAdapter
import com.riard.mynotes.mvp.presenters.FoldersListSettingsPresenter
import com.riard.mynotes.mvp.views.FoldersListSettingsView
import kotlinx.android.synthetic.main.fragment_folders_list_settings.*

class FoldersListSettingsFragment : BaseFragment(), FoldersListSettingsView, FolderAdapter.FolderDialogDelegate {

    private val presenter = FoldersListSettingsPresenter(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_folders_list_settings, container,false)!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        presenter.run {
            setBaseContext(context!!)
            loadFoldersList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.setting_folder_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.menuFolderCreate -> {
                presenter.initFolderCreateDialog()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showFoldersList(folders: ArrayList<String>) {
        tvFoldersListIsEmptyWarning.visibility = View.INVISIBLE
        this.rvFoldersList.apply {
            layoutManager = LinearLayoutManager(context!!)
            adapter = FolderAdapter(folders, this@FoldersListSettingsFragment, true)
        }
    }

    override fun showFoldersListIsEmptyWarning() {
        tvFoldersListIsEmptyWarning.visibility = View.VISIBLE
    }

    override fun deleteFolderByPositionFromUI(position: Int) {
        rvFoldersList.adapter!!.notifyItemRemoved(position)
    }

    override fun insertFolderToEnd(position: Int) {
        rvFoldersList.adapter!!.notifyItemInserted(position)
    }

    override fun selectDialog(folderName: String) {

    }

    override fun deleteFolderByPosition(position: Int) {
        presenter.deleteFolderByPosition(position)
    }
}